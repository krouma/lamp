# lamp
My LAMP stack

This repo contains scripts for provisioning and packaging my Vagrant base box. This box is uploaded on Atlas as [`krouma/lamp`](https://atlas.hashicorp.com/krouma/boxes/lamp).
Content:
* Fedora Cloud 27
* Apache 2.4.29
* PHP 7.1.11
* Xdebug 2.5.5
* MySQL 5.7.20
* VB Guest Additions 5.1.30

Only available provider is `virtualbox`.
