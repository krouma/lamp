#Clean previous build
VM_DIR="$(ls -t ~/VirtualBox\ VMs/ | grep lamp)"
cd ~/VirtualBox\ VMs/$VM_DIR

VBoxManage snapshot $VM_DIR delete clone
rm clone*.vmdk

#Clean unnecessary data
cd ~/Projekty/lamp
rm lamp.box

vagrant up
vagrant ssh -c " \
\
sudo dnf remove -y binutils cpp dkms file gc gcc glibc-devel glibc-headers guile isl kernel-devel kernel-headers libatomic_ops libgomp libmpc libtool-ltdl make; \
sudo dnf clean all; \
\
sudo dd if=/dev/zero of=/EMPTY bs=1M; \
sudo rm -f /EMPTY; \
\
sudo rm -f ~/.bash_history"

vagrant halt

#Shrink virtual hard disk
cd ~/VirtualBox\ VMs/$VM_DIR

VBoxManage snapshot $VM_DIR take clone

SNAPSHOT="$(ls -lt Snapshots | awk '{print $8}')"
SNAPSHOT=${SNAPSHOT:2:36}

VBoxManage clonehd $SNAPSHOT clone.vmdk
VBoxManage storageattach $VM_DIR --storagectl IDE --port 0 --device 0 --type hdd --medium clone.vmdk

cd ~/Projekty/lamp
vagrant package --output lamp.box