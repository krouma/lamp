#!/usr/bin/env bash

# Remove old Vagrant machine
vagrant destroy -f
rm -rf ~/VirtualBox\ VMs/lamp*

# Build new machine
vagrant up --provider virtualbox

# Create box
bin/package.sh