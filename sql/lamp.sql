-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE DATABASE `lamp` /*!40100 DEFAULT CHARACTER SET utf8
  COLLATE utf8_czech_ci */;
USE `lamp`;

DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `article_id`  INT(11) NOT NULL      AUTO_INCREMENT,
  `title`       VARCHAR(255)
                COLLATE utf8_czech_ci DEFAULT NULL,
  `content`     TEXT COLLATE utf8_czech_ci,
  `url`         VARCHAR(255)
                COLLATE utf8_czech_ci DEFAULT NULL,
  `description` VARCHAR(255)
                COLLATE utf8_czech_ci DEFAULT NULL,
  `requestable` TINYINT(1)            DEFAULT NULL,
  `author`      VARCHAR(255)
                COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`article_id`),
  UNIQUE KEY `url` (`url`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_czech_ci;

DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `news_id`     INT(11) NOT NULL      AUTO_INCREMENT,
  `title`       VARCHAR(255)
                COLLATE utf8_czech_ci DEFAULT NULL,
  `content`     TEXT COLLATE utf8_czech_ci,
  `url`         VARCHAR(255)
                COLLATE utf8_czech_ci DEFAULT NULL,
  `description` VARCHAR(255)
                COLLATE utf8_czech_ci DEFAULT NULL,
  `requestable` TINYINT(1)            DEFAULT NULL,
  `author`      VARCHAR(255)
                COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`news_id`),
  UNIQUE KEY `news_url_uindex` (`url`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_czech_ci;

DROP TABLE IF EXISTS `parameters`;
CREATE TABLE `parameters` (
  `key`   VARCHAR(255)
          COLLATE utf8_czech_ci NOT NULL,
  `value` VARCHAR(255)
          COLLATE utf8_czech_ci DEFAULT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_czech_ci;

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id`  INT(11)               NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(255)
             COLLATE utf8_czech_ci NOT NULL,
  `password` VARCHAR(255)
             COLLATE utf8_czech_ci NOT NULL,
  `role`     VARCHAR(255)
             COLLATE utf8_czech_ci NOT NULL DEFAULT 'registered',
  `name`     VARCHAR(255)
             COLLATE utf8_czech_ci          DEFAULT NULL,
  `surname`  VARCHAR(255)
             COLLATE utf8_czech_ci          DEFAULT NULL,
  `nickname` VARCHAR(255)
             COLLATE utf8_czech_ci          DEFAULT NULL,
  `email`    VARCHAR(255)
             COLLATE utf8_czech_ci          DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `user_email_uindex` (`email`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_czech_ci;

CREATE USER 'lamp'@'localhost'
  IDENTIFIED BY PASSWORD '*616447D899FB67566D77F319B512158AD1784460';
GRANT GRANT OPTION, CREATE ROUTINE, CREATE TEMPORARY TABLES, LOCK TABLES, ALTER, CREATE, CREATE VIEW, DELETE, DROP, INDEX, INSERT, REFERENCES, SELECT, SHOW VIEW, TRIGGER, UPDATE, ALTER ROUTINE, EXECUTE ON `lamp`.* TO 'lamp'@'localhost';

-- 2017-03-14 21:05:06
