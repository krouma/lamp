-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

INSERT INTO `article` (`article_id`, `title`, `content`, `url`, `description`, `requestable`, `author`) VALUES
  (1, 'Úvod',
   '<p>V&iacute;tejte na na&scaron;em webu!</p>\n\n<p>Tento web je postaven na <strong>jednoduch&eacute;m redakčn&iacute;m syst&eacute;mu v Nette frameworku</strong>. Toto je &uacute;vodn&iacute; čl&aacute;nek načten&yacute; z datab&aacute;ze.</p>\n',
   'uvod', 'Úvodní článek na webu v Nette v PHP', 1, NULL),
  (2, 'Stránka nebyla nalezena',
   '<p>Litujeme, ale požadovan&aacute; str&aacute;nka nebyla nalezena. Zkontrolujte pros&iacute;m URL adresu.</p>\n',
   'chyba', 'Stránka nebyla nalezena.', 0, NULL);

INSERT INTO `news` (`news_id`, `title`, `content`, `url`, `description`, `requestable`, `author`) VALUES
  (1, 'Novinka nenalezena', '<p>Omlouv&aacute;me se, ale novinka nebyla nalezena.</p>\n', 'chyba', 'Novinka nenalezena',
   0, NULL),
  (4, 'Novinky fungují',
   '<p>Po dlouh&eacute;m tr&aacute;pen&iacute; a nalezen&iacute; sabotuj&iacute;c&iacute;ho středn&iacute;ku již v&scaron;e funguje, jak m&aacute;.</p>\n',
   'novinky-funguji', 'Přidávání novinek již funguje', 1, NULL),
  (5, 'Zkouška redaktora', '<p>Zkou&scaron;ka</p>\n', 'zkouska-redaktora', 'Zkouška redaktora', 0, 'redaktor'),
  (6, 'Skrytá novinka', '<p>Novinka</p>\n', 'skryta', 'Důmyslně ukrytá novinka', 0, 'mates');

INSERT INTO `parameters` (`key`, `value`) VALUES
  ('webName', 'Čtyřiadvacítka Beta'),
  ('webDescription', 'Webové stránky 24. oddílu Hradec Králové'),
  ('installationCompleted', '1');

INSERT INTO `user` (`user_id`, `username`, `password`, `role`, `name`, `surname`, `nickname`, `email`) VALUES
  (1, 'mates', '$2y$10$I.30JI7FBJwkeTOKS6AdgO04hdB3l48aNhosEePw8oYW8SBK0sJ9K', 'admin', '', '', '',
   'kroupa.matyas@gmail.com'),
  (2, 'redaktor', '$2y$10$mUUifMjyOw5xxOaIbp/LNeiKMDPXkGQ1VwEnPPqCHT5P.P1uChl1y', 'editor', NULL, NULL, NULL, NULL),
  (8, 'uzivatel', '$2y$10$G0C0vXrLmvaqg5Fqd.2j4.OmSGce1A4dxnDDV4ydwgGfbzKrHg27i', 'registered', NULL, NULL, NULL,
   'kroupa.maty@spoluzaci.cz');

-- 2017-03-08 14:38:07
